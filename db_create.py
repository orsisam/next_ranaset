#!ranasetvenv/bin/python

from migrate.versioning import api
from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO
from app import db
import os.path


db.create_all()

if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    # create an empty repository at spesified path
    api.create(SQLALCHEMY_MIGRATE_REPO, 'database_repository')
    # mark a database as under this repository's version control
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))

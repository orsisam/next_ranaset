from app import db

class User(db.Model):
    """docstring for User."""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(20), index=True, unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    level = db.Column(db.String(10), nullable=False)
    sekolah_id = db.Column(db.Integer, db.ForeignKey('sekolah.id'))
    asets = db.relationship('Aset', backref='editor', lazy=True)

    def __repr__(self):
        return '<User %r>' % (self.username)

class Sekolah(db.Model):
    """docstring for Sekolah."""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    no_induk = db.Column(db.String(10), unique=True, nullable=False)
    nama_sekolah = db.Column(db.String(50), index=True, unique=True, nullable=False)
    alamat = db.Column(db.String(100), nullable=False)
    no_telp = db.Column(db.String(20), nullable=False)
    users = db.relationship('User', backref='user_name', lazy=True)

    def __repr__(self):
        return "<Sekolah(id='%s', nama_sekolah='%s')>" % (self.id, self.nama_sekolah)

class Aset(db.Model):
    """docstring for Aset"""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    kd_barang = db.Column(db.String(10), nullable=True)
    register = db.Column(db.String(50), nullable=True)
    jenis_barang = db.Column(db.String(50), nullable=False, index=True)
    merk_type = db.Column(db.String(20), nullable=True)
    ukuran_barang = db.Column(db.String(10), nullable=True)
    bahan = db.Column(db.String(15), nullable=True)
    tahun_pembelian = db.Column(db.DateTime, nullable=False)
    terima_barang = db.Column(db.DateTime, nullable=False, index=True)
    toko_penyedia = db.Column(db.String(20), nullable=False, index=True)
    sumberdana = db.Column(db.String(20), nullable=False)
    banyak_barang = db.Column(db.Integer, nullable=False)
    harga_satuan = db.Column(db.Integer, nullable=False)
    jumlah_harga = db.Column(db.Integer, nullable=False)
    tempat_penggunaan = db.Column(db.String(25), nullable=False)
    keterangan = db.Column(db.String(30), nullable=False) # keterangan TBx, TriBulan ke x
    user_id = db.Column(db.Integer, db.ForeignKey('user.id')) # ForeignKey

    def __repr__(self):
        return "<Aset(id='%s', keterangan='%s', user_id='%s')>" % (self.id, self.keterangan, self.user_id)

class UserTest(db.Model):
    """This just an testing tables"""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(10), index=True, unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return "<UserTest(id='%s', username='%s')>" % (self.id, self.username)
        
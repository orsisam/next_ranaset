from flask import render_template, url_for, request, redirect, session
from app import app, db
from .models import Sekolah


@app.route('/login')
def login():
	return render_template('index1.html')

#@app.route('/dologin', methods=['GET', 'POST'])
#def dologin():


'''@app.route('/login2')
def login2():
	return render_template('index2.html')'''

'''@app.route('/login3')
def login3():
	return render_template('index3.html')'''

@app.route('/admin-orig')
def adminorig():
	return render_template('index2_orig.html')

@app.route('/admin-test')
def admintest():
	return render_template('admin-test.html')

#Administrations Block

@app.route('/admin')
def admin():
	return render_template('admin.html', admin=True)


@app.route('/input_school')
def input_school():
	return render_template('admin.html', school_input=True)


@app.route('/inputing_schools', methods=['POST', 'GET'])
def inputing_schools():
	error = None
	if request.method == 'POST':
		in_nama_sekolah = request.form['nama_sekolah']
		in_alamat = request.form['alamat']
		in_no_telp = request.form['no_telp']

		sekolah = Sekolah(nama_sekolah=in_nama_sekolah, alamat=in_alamat, no_telp=in_no_telp)
		db.session.add(sekolah)
		db.session.commit()
		return render_template('admin.html', sukses="transaksi sukses")
	else:
		error = 'transaksi error'

	return render_template('admin.html', error=error)

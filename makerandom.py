#!ranasetvenv/bin/python

from random import randint

def create_randint():
	i = ""
	for x in range(8):
		a = str(randint(0, 10))
		i = i + a
	return i


if __name__ == '__main__':
	text = create_randint()
	print(text)